#! /bin/bash

PROJDIR=$1
PGPASS=~/.pgpass

longhost=`hostname -f`
shorthost=`hostname -s`
conf=${PROJDIR}/xnat_home_${longhost}/config/xnat-conf.properties

user=`cat ${conf} | grep "datasource.username" | cut -d= -f2`
pw=`cat ${conf} | grep "datasource.password" | cut -d= -f2`

echo "${longhost}:5432:${user}:${user}:${pw}" > ${PGPASS}
echo "${shorthost}:5432:${user}:${user}:${pw}" >> ${PGPASS}
echo "localhost:5432:${user}:${user}:${pw}" >> ${PGPASS}
chmod 600 ${PGPASS}
