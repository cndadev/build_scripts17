#!/bin/sh

# Create a puppetized (.erb) InstanceSettings.xml 

# If no arguments are passed, display usage screen
if [ $# == 0 ]
then
	echo Must indicate input and output path and filename
	exit 1
fi

# Scan through user provided arguments.
while [ "$1" != "" ]; do
    case $1 in
        -i | --input )
                shift
                inputFile=$1
                ;;
        -o | --output )
                shift
                outputFile=$1
    esac
    shift
done

if [ -z $inputFile ]; then
    echo Please provide input file name.
    exit 0
fi  
if [ -z $outputFile ]; then
    echo Please provide output file name.
    exit 0
fi   
     
if [ ! -f $inputFile ]; then
    echo Input InstanceSettings.xml does not exist.
    exit 0
else
    if [ -f $outputFile ]; then
        echo Output file already exists. 
        exit 0
    else  
        sed 's/%THUMBNAIL_LOCATION%/<%= thumbnail_path %>/g' $inputFile > $outputFile
        sed -i 's/%CACHE_LOCATION%/<%= cache_path %>/g' $outputFile   
        sed -i 's/%ARCHIVE_LOCATION%/<%= archive_path %>/g' $outputFile
        sed -i 's/%PREARC_LOCATION%/<%= prearchive_path %>/g' $outputFile
        sed -i 's/%LORES_LOCATION%/<%= lores_path %>/g' $outputFile
        sed -i 's/%XDAT_PROJECT%\///g' $outputFile
        sed -i 's/%ADMIN_EMAIL%/<%= admin_email %>/g' $outputFile
        sed -i 's/%SMTP_SERVER%/<%= smtp_server %>/g' $outputFile
        sed -i 's/%PIPELINE_HOME%/<%= pipeline_path %>/g' $outputFile
        sed -i 's/%REQUIRE_LOGIN%/true/g' $outputFile
        sed -i 's/%USER_REGISTRATION%//g' $outputFile
        sed -i 's/%DB_NAME%/<%= db_id %>/g' $outputFile
        sed -i 's/%DB_DRIVER%/org.postgresql.Driver/g' $outputFile
        sed -i 's/%USER%/<%= db_user %>/g' $outputFile
        sed -i 's/%PASSWORD%/<%= db_pass %>/g' $outputFile
        sed -i 's/%DB_URL%/jdbc:postgresql:\/\/<%= db_host %>\/<%= db_name %>/g' $outputFile
        sed -i 's/MaxConnections=\"10\"/MaxConnections=\"<%= db_conns %>\"/g' $outputFile
    fi
fi



