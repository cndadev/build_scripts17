#!/bin/bash

SCRIPTNAME=getBuildFiles.sh

# Usage output for explaining how to use the program
function usage
{
        echo "."
        echo ""
        echo "Usage: ${SCRIPTNAME} [OPTIONS] -cleanRepos"
        echo "   OPTIONAL        "
        echo "-cleanRepos      Delete and reclone all repositories.  Only applies in DEV mode because DEPLOY mode doesn\'t get the repos, just zipped code. -- see buildVars.sh for more info"
        echo ""
}

# Scan through user provided arguments.
while [ "$1" != "" ]; do
        case $1 in
                -cleanRepos )
                        shift
                        cleanRepos=true
                        ;;
                -h | --help | * )
                        usage
                        exit 1
    esac
    shift
done


# Source the build utilities
if [ -s ${CURDIR}/buildUtils.sh ]; then
   source ${CURDIR}/buildUtils.sh || die "Build utility initialization failed."
else
   echo "No buildUtils.sh found."
   exit 1
fi

# Source the build variables
if [ -s ${CURDIR}/buildVars.sh ]; then
   source ${CURDIR}/buildVars.sh || die "Variable initialization failed."
else
   echo "No buildVars.sh found."
   exit 1
fi

echo Running from directory: ${CURDIR}

# If DEPLOY or if DEV and update repos hasn't been chosen, then clean-up the build env and start from scratch
if [ ${BUILD_PURPOSE} = "DEPLOY" ] || [ ! -z ${cleanRepos} ]; then
    echo removing ${BUILDPATH}
    rm -rf ${BUILDPATH} || die "Build directory could not be removed"
fi

if [ ! -d ${BUILDPATH} ]; then
    mkdir ${BUILDPATH} || die "Could not create directory ${BUILDPATH}."
fi

if [ ${BUILD_PURPOSE} = "DEV" ] || [ -z ${MODE} ]; then
   # DEV mode and default to SSH
   export MODE=SSH
fi


# ---- GET XNAT PIPELINES ---

if [ ! -d ${BUILDPATH}/${XNAT_PIPELINE_REPO} ]; then
    cd ${BUILDPATH}
    case ${XNAT_PIPELINE_REPO_SITE} in
    "github")
        case ${MODE} in
        "SSH")
            git clone -b ${XNAT_PIPELINE_REPO_BRANCH} ${SSH_GITHUB}/${XNAT_PIPELINE_REPO_OWN}/${XNAT_PIPELINE_REPO}.git || die "XNAT pipeline retrival failed"
            if [ ! -z ${XNAT_PIPELINE_REPO_REV} ]; then
                pushd ${XNAT_PIPELINE_REPO}
                git checkout ${XNAT_PIPELINE_REPO_BRANCH} ${XNAT_PIPELINE_REPO_REV}
                popd
            fi
            ;;
        *)
            downloadGHRepo ${BUILDPATH} ${XNAT_PIPELINE_REPO} ${XNAT_PIPELINE_REPO_OWN} ${XNAT_PIPELINE_REPO_REV:-master} ;;
        esac
        ;;
    *)
        case ${MODE} in
        "SSH")
            hg clone -r ${XNAT_PIPELINE_REPO_REV:-tip} ${SSH_BITBUCKET}/${XNAT_PIPELINE_REPO_OWN}/${XNAT_PIPELINE_REPO} || die "XNAT pipeline retrival failed" ;;
        *)
            downloadBBRepo ${BUILDPATH} ${XNAT_PIPELINE_REPO} ${XNAT_PIPELINE_REPO_OWN} ${XNAT_PIPELINE_REPO_REV:-tip} ;;
        esac
        ;;
    esac
else
    cd ${BUILDPATH}/${XNAT_PIPELINE_REPO}
    git pull -u || die "XNAT pipeline pull -u failed"
fi
# ---- END GET XNAT PIPELINES ---


# ---- GET CUSTOM PIPELINES IF SPECIFIED ---
if [ ! -z ${CUSTOM_PIPELINE_REPO} ]; then
    if [ -z ${CUSTOM_PIPELINE_REPO_REV} ]; then
        CUSTOM_PIPELINE_REPO_REV=tip
    fi
    if [ ! -d ${BUILDPATH}/${CUSTOM_PIPELINE_REPO} ]; then
        cd ${BUILDPATH}
        case ${MODE} in
          "SSH")
             hg clone -r ${CUSTOM_PIPELINE_REPO_REV} ${SSH_BITBUCKET}/${CUSTOM_PIPELINE_REPO_OWN}/${CUSTOM_PIPELINE_REPO} || die "${CUSTOM_PIPELINE_REPO} retrival failed" ;;
          *)
             downloadBBRepo ${BUILDPATH} ${CUSTOM_PIPELINE_REPO} ${CUSTOM_PIPELINE_REPO_OWN} ${CUSTOM_PIPELINE_REPO_REV} ;;
        esac
    else
        cd ${BUILDPATH}/${CUSTOM_PIPELINE_REPO}
        hg pull -u -r ${CUSTOM_PIPELINE_REPO_REV} || die "${CUSTOM_PIPELINE_REPO} pull -u failed"
    fi
fi
# ---- END GET CUSTOM PIPELINES ---

#---- GET PIPELINE MODULES IF MANIFEST SPECIFIED ---

if [ ! -d ${BUILDPATH}/CUSTOM_PIPELINE_MODULES ]; then
     mkdir ${BUILDPATH}/CUSTOM_PIPELINE_MODULES
fi

# Check current directory first for a manifest.  If that doesn't exist, check the modules directory.
if [ -s ${CURDIR}/pipeline_module_manifest.txt ]; then
    ${CURDIR}/refreshModules.sh -manifest ${CURDIR}/pipeline_module_manifest.txt -dir CUSTOM_PIPELINE_MODULES || die "Could not refresh pipeline modules."
elif [ -s ${BUILDPATH}/${CUSTOM_PIPELINE_REPO}/module_manifest.txt ]; then
    ${CURDIR}/refreshModules.sh -manifest ${BUILDPATH}/${CUSTOM_PIPELINE_REPO}/module_manifest.txt -dir CUSTOM_PIPELINE_MODULES || die "Could not refresh pipeline modules."
else
    echo UNABLE TO UPDATE PIPELINE MODULES.  NO PIPELINE MANIFEST FILE AVAILABLE.
fi

# ---- END GET PIPELINE MODULES ---

