#!/bin/bash

export XNATHOST=`hostname -s`
export PROJECT=`echo ${XNATHOST} | awk -F- '{print $1}'`
BITBUCKET="ssh://hg@bitbucket.org"
SCRIPTS_REPO=build_scripts17
SCRIPTS_REPO_OWN=nrg

SCRIPTNAME="buildNRGXNAT.sh"

# Usage output for explaining how to use the program
function usage
{
        echo ""
        echo "Usage: ${SCRIPTNAME} [OPTIONS] -refreshBuildScripts || -project <NRG_project> -tomcatDown -leaveBuildDir -cleanRepos"  
        echo "   OPTIONAL        "     
        echo "-project               NRG project for which you are running build (cnda, tip, or dca)" 
        echo "-refreshBuildScripts   Download fresh copy of all build scripts from the scripts repository.  All other optional flags ignored."
        echo "-tomcatDown            Build process will not attempt to shutdown tomcat as this flag indicates it is not currently running."
        echo "-leaveBuildDir         Leave XNAT build directory in place in DEPLOY mode \(DEV mode does this automatically\) -- see buildVar.sh for more info"
        echo "-cleanRepos            Delete and reclone all repositories.  Only applies in DEV mode because DEPLOY mode doesn\'t get the repos, just zipped code. -- see buildVars.sh for more info"
        echo ""
}

# Scan through user provided arguments.
rebuildParamsStr=""

while [ "$1" != "" ]; do
        case $1 in
                -project )
                        shift
                        project=$1
                        shift
                        ;;
                -refreshBuildScripts )
                        shift
                        refreshBuildScripts=true
                        ;;
                -tomcatDown )
                        shift
                        tomcatDown="true"
                        rebuildParamsStr="${rebuildParamsStr} -tomcatDown"
                        ;;
                -leaveBuildDir )
                        shift
                        leaveBuildDir=true
                        ;;
                -cleanRepos )
                        shift
                        rebuildParamsStr="${rebuildParamsStr} -cleanRepos"
                        ;;
                -h | --help | * )
                        usage
                        exit 1
    esac
done


echo Running build for project: ${project}
echo rebuiltParamsStr = ${rebuildParamsStr}
echo leaveBuildDir = ${leaveBuildDir}
echo tomcatDown = ${tomcatDown}

if [ ! -z ${project} ]; then
    export PROJECT=${project}    
fi

if [ ${PROJECT} = "cnda" ]; then
    export PROJECT=CNDA
elif [ ${PROJECT} = "dca" ]; then
    export PROJECT=dian
else
    export PROJECT=${PROJECT,,}
fi
echo project: ${PROJECT}

# Script not always run as application user (when puppet runs)
. /data/${PROJECT}/home/.bashrc

export CURDIR="/data/${PROJECT}/home/${PROJECT,,}_build/${XNATHOST}"
echo CURDIR ${CURDIR}

# If either refresh flag was set, download the script repo and refresh the indicated files
if [ ! -z ${refreshBuildScripts} ]; then

   # Retrieve the latest build scripts
   if [ ! -d ${CURDIR}/${SCRIPTS_REPO} ]; then
     cd ${CURDIR}
     hg clone ${BITBUCKET}/${SCRIPTS_REPO_OWN}/${SCRIPTS_REPO}
   else
     cd ${CURDIR}/${SCRIPTS_REPO}
     hg fetch
     cd ${CURDIR}
   fi

   if  [ ! -z ${refreshBuildScripts} ]; then
      # Get a fresh copy of the rest of the build scripts
      cp -rf ${CURDIR}/${SCRIPTS_REPO}/buildXNAT.sh .
      cp -rf ${CURDIR}/${SCRIPTS_REPO}/buildPipelines.sh .
      cp -rf ${CURDIR}/${SCRIPTS_REPO}/buildXNATProperties.sh .
      cp -rf ${CURDIR}/${SCRIPTS_REPO}/buildUtils.sh .
      cp -rf ${CURDIR}/${SCRIPTS_REPO}/buildVars.sh.default .
      cp -rf ${CURDIR}/${SCRIPTS_REPO}/puppetizeInstanceSettings.sh .
      cp -rf ${CURDIR}/${SCRIPTS_REPO}/buildNRGXNAT.sh .
      cp -rf ${CURDIR}/${SCRIPTS_REPO}/buildWebapp.sh .
      cp -rf ${CURDIR}/${SCRIPTS_REPO}/getBuildFiles.sh .
      cp -rf ${CURDIR}/${SCRIPTS_REPO}/maintainPuppetFiles.sh .
      cp -rf ${CURDIR}/${SCRIPTS_REPO}/puppetMaintainedFiles.txt.default .
      echo "Build scripts have been refreshed."
   fi

   dos2unix ${CURDIR}/*.sh
   chmod +x ${CURDIR}/*.sh

else

   # Source the build variables
   if [ -s ${CURDIR}/buildVars.sh ]; then
       source ${CURDIR}/buildVars.sh || die "Variable initialization failed."
   else
       echo "No buildVars.sh found."
       exit 1
   fi

   # Source the build utilities
   if [ -s ${CURDIR}/buildUtils.sh ]; then
       source ${CURDIR}/buildUtils.sh || die "Build utility initialization failed."
   else
       echo "No buildUtils.sh found."
       exit 1
   fi

   # Back up puppet files
   #${CURDIR}/maintainPuppetFiles.sh -backup || die "Could not back up puppet-maintained files." 

   #echo ${tomcatDown}
   #if [ -z ${tomcatDown} ]; then
   #   # Stop tomcat
   #   sudo /sbin/service tomcat6 stop || die "Tomcat shutdown failed."
   #fi   
   
   if [ ${BUILD_PURPOSE} = "DEPLOY" ] && [ -d ${BUILDPATH} ]; then
       rm -rf ${BUILDPATH}
   fi
 
   # Create the build.properties file
   #${CURDIR}/buildXNATProperties.sh || die "Build XNAT properties failed"

   # Run the build
   echo ${CURDIR}/buildXNAT.sh "${rebuildParamsStr}" 
   ${CURDIR}/buildXNAT.sh ${rebuildParamsStr} || die "XNAT build failed." 

   # Restore puppet-maintained files
   #${CURDIR}/maintainPuppetFiles.sh -restore || die "Could not restore puppet-maintained files." 

   # Puppetize the new InstanceSettings.xml
   #if [ -s ${TOMCAT_HOME}/webapps/${APP_NAME}/WEB-INF/conf/InstanceSettings.xml.erb ]; then
   #    rm ${TOMCAT_HOME}/webapps/${APP_NAME}/WEB-INF/conf/InstanceSettings.xml.erb  || die "Couldn't remove InstanceSettings.xml.erb"
   #fi
   #${CURDIR}/puppetizeInstanceSettings.sh -i ${BUILDPATH}/xnat/projects/${APP_NAME}/InstanceSettings.xml -o ${TOMCAT_HOME}/webapps/${APP_NAME}/WEB-INF/conf/InstanceSettings.xml.erb || die "Creation of InstanceSettings.xml.erb file"

   if [ ${BUILD_PURPOSE} = "DEPLOY" ] && [ -z ${leaveBuildDir} ]; then
       rm -rf ${BUILDPATH}
   elif [ ${BUILD_PURPOSE} = "DEV" ]; then
       chmod -R 777 ${BUILDPATH}
   fi

   # Restart tomcat 
   #sudo /sbin/service tomcat7 start || die "Tomcat restart failed"
       
fi
