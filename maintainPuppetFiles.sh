#!/bin/bash

SCRIPTNAME=maintainPuppetFiles.sh

# Usage output for explaining how to use the program
function usage
{
        echo ""
        echo "Usage: ${SCRIPTNAME} [ -backup || -restore ] -archiveLoc"
        echo "-backup        Backup files from Tomcat webapp to archiveLoc (Either backup or restore action required)."
        echo "-restore       Restore files from archiveLoc to Tomcat webapp either backup or restore action required)."
        echo "-archiveLoc    Location where temporary backup files should be stored."
        echo ""
}

# Scan through user provided arguments.
while [ "$1" != "" ]; do
        case $1 in
                -backup )
                        shift
                        backup="true"
                        ;;
                -restore )
                        shift
                        restore="true"
                        ;;
                -archiveLoc )
                        shift
                        archiveLoc=$1
                        ;;
                -h | --help | * )
                        usage
                        exit 1
    esac
    shift
done

if [ -z ${archiveLoc} ]; then
    archiveLoc="${CURDIR}/tempPuppetDir"
fi

# Confirm that one and only one action was chosen
if [ ! -z ${backup} ]; then
    if [ ! -z ${restore} ]; then
         echo "Please choose only one option:  -backup or -restore"
         exit -1
    fi
elif [ -z ${restore} ]; then
    echo "You must select either the -backup or -restore option."
    exit -1
fi

# If a list of puppet maintained files exists
if [ -s ${PUPPET_MAINTAINED_FILES} ] && [ -f ${PUPPET_MAINTAINED_FILES} ]; then
   if [ ! -z ${backup} ]; then
   # Copy off Puppet-maintained files until after build complete
       if [ ! -d ${archiveLoc} ]; then
           mkdir ${archiveLoc} || die "Could not create archive directory ${archiveLoc}"
           mkdir ${archiveLoc}/webapp || die "Could not create archive directory ${archiveLoc}/webapp"
           mkdir ${archiveLoc}/pipeline || die "Could not create archive directory ${archiveLoc}/pipeline"
           while read file
           do
               isWebapp=`echo ${file} | grep webapp`
               isPipeline=`echo ${file} | grep "pipeline:"`
               if [ ! -z ${isWebapp} ]; then
                   webappFile=`echo ${file} | sed 's/webapp://'`
                   cd ${TOMCAT_HOME}/webapps/${APP_NAME}
                   cp --parents ${webappFile} ${archiveLoc}/webapp || echo "Could not back up ${webappFile}."
               elif [ ! -z ${isPipeline} ]; then
                   pipelineFile=`echo ${file} | sed 's/pipeline://'`
                   cd ${PIPELINE_HOME}
                   cp --parents ${pipelineFile} ${archiveLoc}/pipeline || echo "Could not back up ${pipelineFile}."
               fi
           done < ${PUPPET_MAINTAINED_FILES}
           cd ${CURDIR}
       else
	   echo $archiveLoc already exists
       fi
   fi
  
   if [ ! -z ${restore} ]; then
       # Restore Puppet-maintained files
       while read file
       do
           isWebapp=`echo ${file} | grep webapp`
           isPipeline=`echo ${file} | grep "pipeline:"`
           if [ ! -z ${isWebapp} ]; then
               webappFile=`echo ${file} | sed 's/webapp://'`
               cp -rf ${archiveLoc}/webapp/* ${TOMCAT_HOME}/webapps/${APP_NAME} || die "Could not copy puppet file ${webappFile}"
           elif [ ! -z ${isPipeline} ]; then
               pipelineFile=`echo ${file} | sed 's/pipeline://'`
               cp -f ${archiveLoc}/pipeline/* ${PIPELINE_HOME} || die "Could not restore puppet file ${pipelineFile}"
           fi
       done < ${PUPPET_MAINTAINED_FILES}

       # Delete the backed up puppet files only when the entire process has completed successfully.
       rm -rf ${archiveLoc}
       
   fi
   
fi

