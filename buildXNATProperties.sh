#!/bin/bash 

if [ -z ${CURDIR} ]; then
    CURDIR=`pwd`
fi

BUILD_PROPERTIES=${CURDIR}/build.properties 
DATE=`date` 

if [ -f ${BUILD_PROPERTIES} ]; then
    rm ${BUILD_PROPERTIES}
fi
touch ${BUILD_PROPERTIES} 

if [ -z ${MODULE_REPO} ] && [ ! -z ${WEBAPP_MODULE_DIR_DEFAULT} ]; then
    export MODULE_REPO=${WEBAPP_MODULE_DIR_DEFAULT}
else 
    export MODULE_REPO=CUSTOM_WEBAPP_MODULES
fi

echo "# Auto-generated by the NRG XNAT build process ${DATE}" >> ${BUILD_PROPERTIES}
echo "maven.appserver.home=${TOMCAT_HOME}" >> ${BUILD_PROPERTIES}
echo "xdat.project.name=${APP_NAME}" >> ${BUILD_PROPERTIES}
echo "xdat.project.template=xnat" >> ${BUILD_PROPERTIES}
echo "xdat.project.db.name=${DBNAME}" >> ${BUILD_PROPERTIES}
echo "xdat.project.db.driver=org.postgresql.Driver" >> ${BUILD_PROPERTIES}
echo "xdat.project.db.connection.string=jdbc:postgresql://localhost/${DBNAME}" >> ${BUILD_PROPERTIES}
echo "xdat.project.db.user=${DBUSER}" >> ${BUILD_PROPERTIES}
echo "xdat.project.db.password=${PW}" >> ${BUILD_PROPERTIES}
echo "xdat.archive.location=/data/${PROJECT}/archive" >> ${BUILD_PROPERTIES}
echo "xdat.prearchive.location=/data/${PROJECT}/prearchive/" >> ${BUILD_PROPERTIES}
echo "xdat.cache.location=/data/${PROJECT}/cache/" >> ${BUILD_PROPERTIES}
echo "xdat.project.image.thumbnail.location=/data/${PROJECT}/Thumbnail/" >> ${BUILD_PROPERTIES}
echo "xdat.project.image.lo_res.location=/data/${PROJECT}/LoRes/" >> ${BUILD_PROPERTIES}
echo "xdat.mail.server=${MAIL_HOST}" >> ${BUILD_PROPERTIES}
echo "xdat.mail.port=25" >> ${BUILD_PROPERTIES}
echo "xdat.mail.protocol=smtp" >> ${BUILD_PROPERTIES}
echo "xdat.mail.username=" >> ${BUILD_PROPERTIES}
echo "xdat.mail.password=" >> ${BUILD_PROPERTIES}
echo "xdat.mail.prefix=${PROJECT^^}" >> ${BUILD_PROPERTIES}
echo "xdat.mail.admin=${EMAIL_ADDRESS}" >> ${BUILD_PROPERTIES}
echo "xdat.url=https://${FULL_XNATHOST}" >> ${BUILD_PROPERTIES}
echo "xdat.require_login=true" >> ${BUILD_PROPERTIES}
echo "xdat.security.channel=any" >> ${BUILD_PROPERTIES}
echo "xdat.enable_csrf_token=true" >> ${BUILD_PROPERTIES}
echo "xdat.modules.location=${BUILDPATH}/${MODULE_REPO}" >> ${BUILD_PROPERTIES}
echo "xnat.site.title=${SITE_NAME}" >> ${BUILD_PROPERTIES}
