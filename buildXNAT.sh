#!/bin/bash

SCRIPTNAME=buildXNAT.sh

# Usage output for explaining how to use the program
function usage
{
        echo ""
        echo "Usage: ${SCRIPTNAME} [OPTIONS] -tomcatDown"
        echo "   OPTIONAL        "
        echo "-tomcatDown        Build process will not attempt to shutdown tomcat as this flag indicates that tomcat is not running."
        echo "-cleanRepos        Delete and reclone all repositories.  Only applies in DEV mode because DEPLOY mode doesn\'t get the repos, just zipped code. -- see buildVars.sh for more info"
        echo ""
}

# Scan through user provided arguments.
while [ "$1" != "" ]; do
        case $1 in
                -tomcatDown )
                        shift
                        tomcatDown=true
                        ;;
                -cleanRepos )
                        shift
                        getBuildStr=-cleanRepos
                        ;;
                -h | --help | * )
                        usage
                        exit 1
    esac
    shift
done

# Source the build variables
if [ -s ${CURDIR}/buildVars.sh ]; then
   source ${CURDIR}/buildVars.sh || die "Variable initialization failed."
else
   echo "No buildVars.sh found."
   exit 1
fi

# Source the build utilities 
if [ -s ${CURDIR}/buildUtils.sh ]; then
   source ${CURDIR}/buildUtils.sh || die "Build utility initialization failed."
else
   echo "No buildUtils.sh found."
   exit 1
fi

echo Running from directory: ${CURDIR} 

# Retrieve build files
${CURDIR}/getBuildFiles.sh ${getBuildStr} || die "Get build files failed."

# To run this script directly (as opposed to from buildNRGXNAT.sh), uncomment the following if statement.
#if [ -z ${tomcatDown} ]; then
  # Stop tomcat
#  sudo /sbin/service tomcat6 stop || die "Tomcat shutdown failed"
#fi

# Build the new XNAT deploy
# echo "Building and deploying updated ${APP_NAME} webapp"
# ${CURDIR}/buildWebapp.sh  || die "XNAT webapp build failed"

# Run custom script to perform post webapp build steps, if script exists
#if [ -s ${CURDIR}/runPostWebappBuildSteps.sh ]; then
#    ${CURDIR}/runPostWebappBuildSteps.sh || die "XNAT webapp post-build steps failed"
#fi 

# Build the pipelines
${CURDIR}/buildPipelines.sh || die "Build pipelines failed."

# To run this script directly (as opposed to from buildNRGXNAT.sh), uncomment the following statement.
#sudo /sbin/service tomcat6 start || die "Tomcat restart failed"

