#!/bin/bash

# Script assumes the following:
#  1. Script checks to see if this path exists, and tries get it if doesn't already exist: ${BUILDPATH}/${XNAT_PIPELINE_REPO}
#  2. Script checks to see if this path exists, and tries get it if it is specified and doesn't already exist: ${BUILDPATH}/${CUSTOM_PIPELINE_REPO}
#  3. Script assumes this file exists: ${TOMCAT_HOME}/webapps/${APP_NAME}/WEB-INF/lib/xdat-beans*.jar

if [ -z ${CURDIR} ];then

    if [ -s ./buildVars.sh ]; then
        source ./buildVars.sh
    else
        echo "No buildVars.sh found in current directory."
    fi

    if [ -s ./buildUtils.sh ]; then
        source ./buildUtils.sh
    else
        echo "No buildUtils.sh found in current directory."
    fi
fi

rm -rf ${BUILDPATH}/pipeline
rm -rf ${PIPELINE_HOME}/*


# If either expected source path  doesn't exist, then go fetch it
if [ ! -d ${BUILDPATH}/${XNAT_PIPELINE_REPO} ] || ([ ! -z ${CUSTOM_PIPELINE_REPO} ] && [ ! -d ${BUILDPATH}/${CUSTOM_PIPELINE_REPO} ]); then
    ${CURDIR}/getBuildFiles.sh
fi

mkdir ${BUILDPATH}/pipeline
cp -rf ${BUILDPATH}/${XNAT_PIPELINE_REPO}/* ${BUILDPATH}/pipeline

# If custom pipeline repo is specified, copy the custom repo on top of xnat repo
if [ ! -z ${CUSTOM_PIPELINE_REPO} ]; then
    cp -rf ${BUILDPATH}/${CUSTOM_PIPELINE_REPO}/* ${BUILDPATH}/pipeline
fi


# Copy xdat-beans.jar into the pipeline lib dir
if [ ! -d ${BUILDPATH}/pipeline/lib ]; then
    mkdir ${BUILDPATH}/pipeline/lib
fi
cp -f ${BUILDPATH}/xnat/pipeline/lib/xdat-beans*.jar ${BUILDPATH}/pipeline/lib

# Build the gradle.properties file if it does not already exist
if [ ! -e ${BUILDPATH}/pipeline/gradle.properties ]; then
    touch ${BUILDPATH}/pipeline/gradle.properties
    echo "xnatUrl=https://${FULL_XNATHOST}" >> ${BUILDPATH}/pipeline/gradle.properties
    echo "siteName=${SITE_NAME}" >> ${BUILDPATH}/pipeline/gradle.properties
    echo "adminEmail=${ADMIN_EMAIL}" >> ${BUILDPATH}/pipeline/gradle.properties
    echo "smtpServer=${SMTP_HOST}" >> ${BUILDPATH}/pipeline/gradle.properties
    echo "destination=${PIPELINE_HOME}" >> ${BUILDPATH}/pipeline/gradle.properties
    if [ -d ${BUILDPATH}/CUSTOM_PIPELINE_MODULES ]; then
        echo "modulePaths=${BUILDPATH}/CUSTOM_PIPELINE_MODULES" >> ${BUILDPATH}/pipeline/gradle.properties
    fi
fi

# Build the pipelines
pushd ${BUILDPATH}/pipeline
./gradlew || die "Pipeline build process failed"
popd
