#!/bin/bash

SCRIPTNAME=refreshModules.sh

# Usage output for explaining how to use the program
function usage
{
        echo "."
        echo ""
        echo "Usage: ${SCRIPTNAME} [REQUIRED] -manifest pathToManifest -dir moduleDirectory [OPTIONS] -zip"
        echo "-manifest        Flag followed by fully qualified path to manifest file."
        echo "-dir             Flag followed by directory where modules will be staged." 
        echo "   OPTIONAL        "
        echo "-zip             If -zip flag exists, then modules will be zipped inside the directory indicated by dir.  Otherwise, they will be in directories."
        echo ""
}

# Scan through user provided arguments.
while [ "$1" != "" ]; do
   case $1 in
                -manifest )
                        shift
                        manifest=$1
                        ;;
                -dir )
                        shift
                        dir=$1
                        ;;
                -zip )
                        zip=true 
                        ;;
                -h | --help | * )
                        usage
                        exit 1
    esac
    shift
done

echo manifest $manifest
echo dir $dir
echo zip $zip

if [ -z ${BUILDPATH} ] && [ -s ./buildVars.sh ] && [ -s ./buildUtils.sh ]; then
    source ./buildVars.sh
    source ./buildUtils.sh
elif [ -z ${BUILDPATH} ]; then
    echo Could not find ./buildVars.sh and/or ./buildUtils.sh.  Please place them in execution directory and try again.
    exit 1
fi

if [ ! -z ${BU} ] && [ ! -z ${BP} ]; then
   CURL_ARGS="-u $BU:$BP"
fi

if [ ! -z ${zip} ]; then
   REPO_TARGET=${BUILDPATH}
else
   REPO_TARGET=${BUILDPATH}/${dir}
fi

# Cleaning out modules directory
if [ -d /tmp/${APP_NAME}_modules ]; then
    rm -rf /tmp/${APP_NAME}_modules || die "Couldn't delete /tmp/${APP_NAME}_modules"
fi

if [ -s ${manifest} ]; then
   while read source_repo
   do
       repo=`echo $source_repo | awk -F/ '{print $2}'`
       repoOwn=`echo ${source_repo} | awk -F/ '{print $1}'`
       repoRev=`echo ${source_repo} | awk -F/ '{print $3}'`
       
       if [ -z ${repoRev} ]; then
           repoRev=tip
       fi      
 
       if [ "${repoRev}" = "tip" ]; then
           bbJson="Rate limit for this resource has been exceeded"
           while [ "${bbJson}" = "Rate limit for this resource has been exceeded" ]; do
               sleep 6 
               bbJson=`curl ${CURL_ARGS} -s "${HTTPS_BITBUCKET}/api/2.0/repositories/${repoOwn}/${repo}/commit/tip"`
               if [ "${bbJson}" = "Rate limit for this resource has been exceeded" ]; then
                   echo "${bbJson}."
                   echo "Waiting on Bitbucket rate limit to time out."
               fi
           done
           if [ -z "${bbJson}" ]; then
               repoRev=tip
               echo Tip changeset is unknown. SSH mode still has a chance to find it. HTTPS will fail.
           else
               #repoRev=`python -c "bbJson=${bbJson}; print bbJson['hash'][:12]"`
               repoRev=`echo ${bbJson} | awk  -F '\"hash\":' '{print $2}' | cut -d\" -f2 | cut -c1-12`
               echo "Hash of ${repoOwn}/${repo} tip: ${repoRev}"      
           fi 
       fi
        
       echo repoRev: ${repoRev}
        
       if [ ! -z ${zip} ]; then 
           if  [ -s ${BUILDPATH}/${dir}/${repoOwn}-${repo}-${repoRev}.zip ]; then
               echo Module ${repoOwn}/${repo}/${repoRev} is already up to date.
               continue
           elif [ -s ${BUILDPATH}/${dir}/${repoOwn}-${repo}-*.zip ]; then 
               rm ${BUILDPATH}/${dir}/${repoOwn}-${repo}-*.zip
           fi
       fi

       cd ${REPO_TARGET}           

       if [ -z ${MODE} ] || [ "${MODE}" = "SSH" ]; then
            if [ ! -d ${repo} ]; then
                echo hg clone -r ${repoRev} ${SSH_BITBUCKET}/${repoOwn}/${repo}
                hg clone -r ${repoRev} ${SSH_BITBUCKET}/${repoOwn}/${repo} || die "Could not download ${repo} ${repoOwn} ${repoRev}"
            else
                cd ${repo}
                hg pull -u -r ${repoRev} ${SSH_BITBUCKET}/${repoOwn}/${repo} || die "Could not download ${repo} ${repoOwn} ${repoRev}"
            fi
            if [ "${repoRev}" = "tip" ]; then
                cd ${REPO_TARGET}/${repo}
                repoRev=`hg tip | head -1 | cut -d: -f3`
                echo looking for: ${BUILDPATH}/${dir}/${repoOwn}-${repo}-${repoRev}.zip
                # Check again to see if can exit or delete old file
                if [ ! -z ${zip} ]; then
                    if  [ -s ${BUILDPATH}/${dir}/${repoOwn}-${repo}-${repoRev}.zip ]; then
                        echo Module ${repoOwn}/${repo}/${repoRev} is already up to date.
                        continue
                    elif [ -s ${BUILDPATH}/${dir}/${repoOwn}-${repo}-*.zip ]; then
                        echo Removing older versions of module ${BUILDPATH}/${dir}/${repoOwn}-${repo}-*.zip
                        rm ${BUILDPATH}/${dir}/${repoOwn}-${repo}-*.zip
                    fi
                fi
            fi
        else
            downloadBBRepo ${REPO_TARGET} ${repo} ${repoOwn} ${repoRev} || die "Could not download ${repo} ${repoOwn} ${repoRev}"
        fi

        if [ ! -z ${zip} ]; then
            MVN_BUILD=""                   
            cd ${REPO_TARGET}/${repo}
            if [ -s ./pom.xml ]; then 
                  MVN_BUILD=`cat ./pom.xml | grep "<modules>"`
            fi
            if [ ! -z ${MVN_BUILD} ]; then 
                mvn -Ddependency.locations.enabled=false -DfailIfNoTests=false -e clean package -DskipTests || die "Maven build failed for ${repoOwn}/${repo}"
                for h in `cat ./pom.xml | grep "<module>" | sed 's/<module>//g;s/<\/module>//g'`
                do
                    modulename=`echo $h | sed 's/ //g'`
                    echo "Found module jar ${modulename}"
                    jarpath=`find . -name "*${modulename}*.jar"`
                    if [ -z ${jarpath} ]; then
                        die "Maven ${repoOwn}/${repo} jars could not be found."
                    fi
                    cp ${jarpath} ${REPO_TARGET}/${dir}
                done
            else
                echo zipping to ${REPO_TARGET}/${dir}  
                zip -r ${REPO_TARGET}/${dir}/${repoOwn}-${repo}-${repoRev}.zip * || die "Failed to zip module ${repoOwn}/${repo}"
            fi
        fi     
   done < ${manifest}
else
   echo "Could not find ${manifest}."
fi
