#!/bin/bash

TMPSQL=./tmp.sql
#HOST=`hostname | cut -d. -f1`
export PGPASSFILE=~/.pgpass

if [ -z $1 ]; then
   echo No user id provided.
   exit 1
else
   echo "update xdat_user set enabled=1, verified=1 where login = '$1';" >> ${TMPSQL}
   echo "delete from xs_item_cache where contents like '%$1%';" >> ${TMPSQL}
   psql -f ${TMPSQL}
   rm ${TMPSQL} 
fi 
